module github.com/armadillica/svn-manager

go 1.12

require (
	github.com/armadillica/flamenco-sync-server v0.0.0-20181122091614-a58555a64540
	github.com/foomo/htpasswd v0.0.0-20180422071726-cb63c4ac0e50
	github.com/golang/mock v1.6.0
	github.com/gorilla/mux v1.7.2
	github.com/kardianos/osext v0.0.0-20190222173326-2bc1f35cddc0
	github.com/kr/pretty v0.1.0 // indirect
	github.com/sirupsen/logrus v1.4.2
	github.com/stretchr/testify v1.3.0
	github.com/xeipuuv/gojsonpointer v0.0.0-20180127040702-4e3ac2762d5f // indirect
	github.com/xeipuuv/gojsonreference v0.0.0-20180127040603-bd5ef7bd5415 // indirect
	github.com/xeipuuv/gojsonschema v1.1.0
	gopkg.in/check.v1 v1.0.0-20180628173108-788fd7840127
	gopkg.in/yaml.v2 v2.2.2
)
