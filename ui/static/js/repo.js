document.addEventListener('DOMContentLoaded', function(){
  const bcrypt = dcodeIO.bcrypt;
  const salt = bcrypt.genSaltSync(10).replace(/\$2a\$/, '$2y$');

  const deleteRepositoryForm = document.getElementById('deleteRepositoryForm');
  const deleteRepositoryModal = document.getElementById('deleteRepositoryModal');
  const changeAccessModal = document.getElementById('changeAccessModal');
  const grantAccessModal = document.getElementById('grantAccessModal');
  const revokeAccessModal = document.getElementById('revokeAccessModal');

  const grantAccessInputs = grantAccessModal.getElementsByTagName('input');
  const deleteRepositoryInputs = deleteRepositoryModal.getElementsByTagName('input');
  const changeAccessInputs = changeAccessModal.getElementsByTagName('input');
  const revokeAccessInputs = revokeAccessModal.getElementsByTagName('input');

  function generatePassword() {
    return Math.random().toString(36).slice(2) + Math.random().toString(36).slice(2);
  }

  function hashPassword(value) {
    return bcrypt.hashSync(value, salt);
  }

  // Grant Access and Change Access use the same API call but slightly different UI.
  function grantChangeAccess(inputs) {
    return function(event) {
      event.preventDefault();

      clearErrors(event.target);

      const password = inputs['password'].value;
      const repoID = inputs['repo-id'].value;
      const username = inputs['username'].value;

      const passwordHashed = hashPassword(password);

      const headers = {
        'Content-Type': 'application/json',
        'Accept': 'application/json'
      };
      const data = {
        'grant': [{
          'username': username,
          'password': passwordHashed
        }]
      };
      const baseErrorText = 'Unable to grant access to repository';
      fetch(`/api/repo/${repoID}/access`, {
        method: 'POST',
        headers: headers,
        body: JSON.stringify(data)
      })
        .then(result => {
          if (result.status === 200) {
            window.location.reload();
          } else {
            handleErrors(event.target, result, baseErrorText);
          }
        })
        .catch(error => {
          // console.error(baseErrorText, error);
          showErrors(event.target, `${baseErrorText}: "${error}"`);
        });
    };
  }

  function onOpenChangeAccess(username) {
    return function() {
      changeAccessInputs['username'].value = username;
      // Generate a random password when a modal for adding a user is opened.
      changeAccessInputs['password'].value = generatePassword();
    };
  }

  function onOpenRevokeAccess(username) {
    return function() {
      revokeAccessInputs['username'].value = username;
    };
  }

  deleteRepositoryForm.addEventListener('click', function () {
    // Reset validity in case the button is clicked again
    const confirmedRepoIDEl = deleteRepositoryInputs['confirm-repo-id'];
    clearValidity(confirmedRepoIDEl);
  });

  deleteRepositoryForm.addEventListener('submit', function (event) {
    event.preventDefault();

    const confirmedRepoIDEl = deleteRepositoryInputs['confirm-repo-id'];
    clearErrors(event.target);
    clearValidity(confirmedRepoIDEl);

    const hiddenRepoID = deleteRepositoryInputs['repo-id'].value;
    const confirmedRepoID = confirmedRepoIDEl.value;
    let validityMessage = '';
    if (!hiddenRepoID || !confirmedRepoID || hiddenRepoID !== confirmedRepoID) {
      validityMessage = 'Please confirm deletion by entering repository ID';
    }
    confirmedRepoIDEl.setCustomValidity(validityMessage);
    confirmedRepoIDEl.reportValidity();
    if (validityMessage) {
      return false;
    }
    const headers = {
      'Content-Type': 'application/json',
      'Accept': 'application/json'
    };
    const baseErrorText = 'Unable to delete repository';
    fetch(`/api/repo/${confirmedRepoID}`, {
      method: 'DELETE',
      headers: headers
    })
      .then(result => {
        if (result.status === 204) {
          window.location.href = '/manage/';
        } else {
          handleErrors(event.target, result, baseErrorText);
        }
      })
      .catch(error => {
        // console.error(baseErrorText, error);
        showErrors(event.target, `${baseErrorText}: "${error}"`);
      });
  });

  document.getElementById('openGrantAccessModalBtn').addEventListener('click', function() {
    // Generate a random password when a modal for adding a user is opened.
    const passwordEl = grantAccessInputs['password'];
    passwordEl.value = generatePassword();
  });

  Array.prototype.forEach.call(document.getElementsByClassName('btn-change-access'), function(el) {
    el.addEventListener('click', onOpenChangeAccess(el.dataset.username));
  });

  Array.prototype.forEach.call(document.getElementsByClassName('btn-revoke-access'), function(el) {
    el.addEventListener('click', onOpenRevokeAccess(el.dataset.username));
  });

  document.getElementById('revokeAccessForm').addEventListener('submit', function(event) {
    event.preventDefault();

    const repoID = revokeAccessInputs['repo-id'].value;
    const username = revokeAccessInputs['username'].value;

    clearErrors(event.target);

    const headers = {
      'Content-Type': 'application/json',
      'Accept': 'application/json'
    };
    const data = {'revoke': [username]};
    const baseErrorText = 'Unable to revoke access to repository';
    fetch(`/api/repo/${repoID}/access`, {
      method: 'POST',
      headers: headers,
      body: JSON.stringify(data)
    })
      .then(result => {
        console.log('then:', result);
        if (result.status === 200) {
          window.location.reload();
        } else {
          handleErrors(event.target, result, baseErrorText);
        }
      })
      .catch(error => {
        // console.error(baseErrorText, error);
        showErrors(event.target, `${baseErrorText}: "${error}"`);
      });
  });

  document.getElementById('grantAccessForm').addEventListener(
    'submit', grantChangeAccess(grantAccessInputs)
  );

  document.getElementById('changeAccessForm').addEventListener(
    'submit', grantChangeAccess(changeAccessInputs)
  );
});
